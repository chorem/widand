Compilation
===========

./gradlew clean assemble

apk is in build/outputs/apk directory

Release
=======

Last compiled version can be found at http://widand.chorem.com/WidAnd-debug.apk

Other
=====

Source du POC d'agent Wid.

Agent wid pour collecter et envoyer à Wid les informations disponibles
sur un appareil android:
- applications utilisées
- Position géographique
- Nombre de pas
- Fréquence cardiaque
- appels (reçus, émis, manqués)

TODO (dans l'ordre d'importance)
--------------------------------

- faire un code source plus proche des bonnes pratiques android
  - ajout de services / receivers
  - ne pas tout lancer depuis WidAgent (surtout si l'utilisateur
    ne souhaite pas tracker les applications)
- calculer la surcharge de consommation de la baterrie pour
  - l'application
  - chaque composant (application, location, step, heatbeat)
