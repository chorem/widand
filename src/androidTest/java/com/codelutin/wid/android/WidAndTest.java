package com.codelutin.wid.android;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.codelutin.wid.android.WidAndTest \
 * com.codelutin.wid.android.tests/android.test.InstrumentationTestRunner
 */
public class WidAndTest extends ActivityInstrumentationTestCase2<WidAnd> {

    public WidAndTest() {
        super("com.codelutin.wid.android", WidAnd.class);
    }

}
