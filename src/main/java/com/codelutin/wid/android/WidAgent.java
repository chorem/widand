package com.codelutin.wid.android;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.support.v7.app.NotificationCompat;
import android.text.format.DateUtils;

public class WidAgent extends AccessibilityService {

    private WidDbHelper dbHelper;
    private SharedPreferences config;

    public static WidLocationHelper locationHelper;
    public static WidStepHelper stepHelper;
    public static WidHeartRateHelper heartrateHelper;
    public static WidAgent widAgent;

    private long lastDate;
    private String lastApp;
    private String lastCommand;

     //Create broadcast object
    BroadcastReceiver mybroadcast = new BroadcastReceiver() {
        //When Event is published, onReceive method is called
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    Log.i("Wid CurrentActivity", "Screen OFF");
                    addEvent(null, null);

                    Intent autoSync = new Intent(context, WidAutoSyncService.class);
                    context.startService(autoSync);
                }
            } catch (Exception eee) {
                Log.e("Wid WidAgent ERROR", eee.getMessage(), eee);
            }
        }
    };

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            locationHelper.untrackLocation();
            stepHelper.untrackStep();
            heartrateHelper.untrackHeartRate();
        } catch (Exception eee) {
            Log.e("Wid WidAgent ERROR", eee.getMessage(), eee);
        }
    }


    @Override
    protected void onServiceConnected() {
        try {
            super.onServiceConnected();
            widAgent = this;

            config = PreferenceManager.getDefaultSharedPreferences(this);
            WidAnd.DEVICE_NAME = config.getString("deviceName", "phone");

            //Configure these here for compatibility with API 13 and below.
            AccessibilityServiceInfo serviceConfig = new AccessibilityServiceInfo();
            serviceConfig.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
            serviceConfig.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;

            if (Build.VERSION.SDK_INT >= 16)
                //Just in case this helps
                serviceConfig.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;

            setServiceInfo(serviceConfig);

            registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_OFF));
            dbHelper = new WidDbHelper(this);

            locationHelper = new WidLocationHelper(this);
            locationHelper.trackLocation();

            stepHelper = new WidStepHelper(this);
            stepHelper.trackStep();

            heartrateHelper = new WidHeartRateHelper(this);
            heartrateHelper.trackHeartRate();

            createNotification();
        } catch (Exception eee) {
            Log.e("Wid WidAgent ERROR", eee.getMessage(), eee);
        }
    }

    public void createNotification() {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(0);

        String title = "";

        String startStopTag = config.getString("startStopTag", "");
        String[] startStopTagArray = startStopTag.split(";");

        Log.d("Wid WidAgent", "create notification:" + startStopTag);

        Intent widandUI = new Intent(this, WidAnd.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, -1,
                widandUI, 0);

        NotificationCompat.MediaStyle mediaStyle = new NotificationCompat.MediaStyle();
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this);
        notifBuilder.setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                // Show controls on lock screen even when user hides sensitive content.
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setOngoing(true)
                .setSmallIcon(R.drawable.widlogo)
                // Apply the media style template
                .setStyle(mediaStyle)
                .setContentText(startStopTag);

        int i=0;
        for (String tag : startStopTagArray) {
            if (tag != null && !tag.trim().equals("")) {
                long date = config.getLong(tag, 0);
                String action;
                int icon;
                if (date == 0) {
                    action = "start";
                    icon = android.R.drawable.star_off;
                } else {
                    action = "stop";
                    icon = android.R.drawable.star_on;
                }

                Intent myIntent = new Intent(this, WidStartStopService.class);
                myIntent.putExtra("action", action);
                myIntent.putExtra("tag", tag);
                PendingIntent intent = PendingIntent.getService(
                        this, i, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notifBuilder.addAction(icon, action + ":" + tag, intent);

                Log.d("Wid WidAgent", "create notification -> " + tag + ":" + action + ":" + date);

                if (i == 0 || date > 0) {
                    mediaStyle.setShowActionsInCompactView(i);
                    title = action + " '" + tag + "'";
                    if (date > 0) {
                        title += "(" + DateUtils.formatDateTime(this, date,
                                DateUtils.FORMAT_SHOW_TIME + DateUtils.FORMAT_NUMERIC_DATE)
                                + ")";
                    }
                }
                i++;
            }
        }
        if (i > 0) {
            notifBuilder.setContentTitle("Wid " + title);

            Notification notification = notifBuilder.build();
            notificationManager.notify(0, notification);
        }
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        try {
            if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
                ComponentName componentName = new ComponentName(
                        event.getPackageName().toString(),
                        event.getClassName().toString()
                );

                ActivityInfo activityInfo = tryGetActivity(componentName);
                boolean isActivity = activityInfo != null;
                if (isActivity) {
                    String command = componentName.getPackageName();
                    String name;
                    try {
                        name = activityInfo.applicationInfo.loadLabel(getPackageManager()).toString();
                    } catch (Exception eee) {
                        Log.e("Wid error get name", eee.getMessage(), eee);
                        name = "";
                    }
                    addEvent(command, name);
                }
            }
        } catch (Exception eee) {
            Log.e("Wid WidAgent ERROR", eee.getMessage(), eee);
        }
    }

    private ActivityInfo tryGetActivity(ComponentName componentName) {
        try {
            return getPackageManager().getActivityInfo(componentName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    @Override
    public void onInterrupt() {}

    private void addEvent(String command, String appName) {
        if (command == null || !command.equals(lastCommand)) {
            long now = System.currentTimeMillis();
            if (lastCommand != null && lastDate < now) {
                // save to database currentApp
                if (config.getBoolean("application", true)) {
                    dbHelper.addEntry(lastDate, now, "application", "command", lastCommand, "name", lastApp);
                }
            }
            this.lastApp = appName;
            this.lastCommand = command;
            this.lastDate = now;

            Log.d("Wid CurrentActivity", String.valueOf(appName));
        }
    }
}