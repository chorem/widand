package com.codelutin.wid.android;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

public class WidAnd extends Activity {

    public static final String APP_NAME = "widand";
    public static String DEVICE_NAME = "phone";

    private WidSyncHelper syncHelper;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("Wid main", "created");
        super.onCreate(savedInstanceState);

        syncHelper = new WidSyncHelper(this);

        setContentView(R.layout.main);

        refresh();
    }

    private void setText(String s) {
        TextView editText = (TextView) findViewById(R.id.text);
        editText.setText(s);
    }

    private void refresh() {
        StringBuilder json = new StringBuilder();
        try {
            syncHelper.getJson(json);
        } catch (Exception eee) {
            Log.e("Wid XXX error", eee.getMessage(), eee);
        }

        if ("[]".equals(json.toString())) {
            json.append("\nIf is not already done, please go to\nconfiguration->accessibility\nand activate 'Wid Service'.");
        }

        setText(json.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * react to the user tapping/selecting an options menu item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_preference:
                //Toast.makeText(this, "ADD!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, WidPreferencesActivity.class);
                startActivity(i);
                return true;
            case R.id.menu_item_sync:
                Intent sync = new Intent(this, WidAutoSyncService.class);
                startService(sync);
                // TODO get result of sync and add it to text
                setText("");

                // don't work with compat lib, can't fetch URL in forground
//                String result = syncHelper.sync(true);
//                setText(result);
                return true;
            case R.id.menu_item_refresh:
                refresh();
                return true;
            case R.id.menu_item_sensorsList:
                String s = "device id:" + Settings.Secure.ANDROID_ID + "\n\n";

                LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                List<String> locationProviders = mLocationManager.getAllProviders();
                for (String p : locationProviders) {
                    Location location = mLocationManager.getLastKnownLocation(p);
                    s += "location provider " + p + " active:" + mLocationManager.isProviderEnabled(p) + " location:" + location + "\n";
                }

                s +="\n";

                SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
                List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
                for(Sensor mSensor : deviceSensors) {
                    s += " " + mSensor.getName() + "(" + mSensor.getVendor() +")" + "(" + mSensor.getVersion()+")\n";
                }

                if (mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null){
                    s += "\nTemperature sensor available :)\n";
                }

                setText(s);
                return true;
            case R.id.menu_item_clearData:
                WidDbHelper dbHelper = new WidDbHelper(this);
                dbHelper.deleteAllEntries();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
