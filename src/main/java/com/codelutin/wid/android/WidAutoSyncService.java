package com.codelutin.wid.android;


import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidAutoSyncService extends IntentService {

    private WidSyncHelper syncHelper;

    public WidAutoSyncService() {
        super("WidAutoSyncService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        syncHelper = new WidSyncHelper(this);
        Log.d("Wid autosync", "created");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String result = syncHelper.sync(false);
            Log.d("Wid autosync", "result:" + result);
        } catch (Exception eee) {
            Log.e("Wid autosync ERROR", eee.getMessage(), eee);
        }
    }


}
