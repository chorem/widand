package com.codelutin.wid.android;

import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidCalendarHelper {

    private Context context;

    public WidCalendarHelper(Context context) {
        this.context = context;
    }

    public void getCalendarEvents(long dateBegin, long dateEnd, StringBuilder json) {
        String projection[]=new String[] {
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.DESCRIPTION
        };

        // Define 'where' part of query.
        String selection = CalendarContract.Events.DTSTART + " > ? AND " + CalendarContract.Events.DTSTART + "<= ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(dateBegin), String.valueOf(dateEnd)};


        Cursor cursor = context.getContentResolver().query(
                CalendarContract.Events.CONTENT_URI, projection,
                selection, selectionArgs, CalendarContract.Events.DTSTART + " ASC");

        String sep = json.length() > 0 ? "," : "";

        while (cursor.moveToNext()) {
            final String title = cursor.getString(0);
            final long begin = cursor.getLong(1);
            final long end = cursor.getLong(2);
            final String description = cursor.getString(3);

            String event = WidUtils.toJSON(
                    begin, end, "calendar",
                    "title", title, "description", description);
            json.append(sep);
            json.append(event);
            sep = ",\n";
        }

        cursor.close();
    }


}
