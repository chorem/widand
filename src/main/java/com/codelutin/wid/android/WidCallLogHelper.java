package com.codelutin.wid.android;

import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidCallLogHelper {

    private Context context;

    public WidCallLogHelper(Context context) {
        this.context = context;
    }
    
    public void getCallDetails(long dateBegin, long dateEnd, StringBuilder json) {
        String projection[]=new String[] {
            CallLog.Calls._ID,
            CallLog.Calls.NUMBER,
            CallLog.Calls.CACHED_NAME,
            CallLog.Calls.DATE,
            CallLog.Calls.DURATION,
            CallLog.Calls.TYPE};

        // Define 'where' part of query.
        String selection = CallLog.Calls.DATE + " > ? AND " + CallLog.Calls.DATE + "<= ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(dateBegin), String.valueOf(dateEnd)};


        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                projection, selection, selectionArgs, CallLog.Calls.DATE + " ASC");

        int numberIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int nameIndex = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int typeIndex = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int dateIndex = cursor.getColumnIndex(CallLog.Calls.DATE);
        int durationIndex = cursor.getColumnIndex(CallLog.Calls.DURATION);

        String sep = json.length() > 0 ? "," : "";

        while (cursor.moveToNext()) {
            String number = cursor.getString(numberIndex);
            String name = cursor.getString(nameIndex);
            String callType = cursor.getString(typeIndex);
            Long callDate = cursor.getLong(dateIndex);
            Long callDuration = cursor.getLong(durationIndex);

            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "outgoing";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "incoming";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "missed";
                    break;
            }

            String event = WidUtils.toJSON(
                    callDate, callDate + callDuration * 1000, "call",
                    "type", dir, "number", number, "name", name);
            json.append(sep);
            json.append(event);
            sep = ",\n";

        }
        cursor.close();
    }
}
