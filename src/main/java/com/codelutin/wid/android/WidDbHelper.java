package com.codelutin.wid.android;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Log;
import java.util.Date;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = WidAnd.APP_NAME + ".db";

    private SharedPreferences config;

    /* Inner class that defines the table contents */
    public static abstract class EventEntry implements BaseColumns {
        public static final String TABLE_NAME = "event";
        public static final String COLUMN_NAME_DATE= "date";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_DATA = "data";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String DATE_TYPE = " INTEGER";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + EventEntry.TABLE_NAME + " (" +
            EventEntry._ID + " INTEGER PRIMARY KEY," +
            EventEntry.COLUMN_NAME_DATE + DATE_TYPE + COMMA_SEP +
            EventEntry.COLUMN_NAME_DURATION + INT_TYPE + COMMA_SEP +
            EventEntry.COLUMN_NAME_DATA + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + EventEntry.TABLE_NAME;

    public WidDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        config = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.i("Wid WidDbHelper", "onCreate");
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("Wid WidDbHelper", "onUpgrade");
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("Wid WidDbHelper", "onDowngrade");
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addEntry(long dateBegin, long dateEnd, String type, Object ...values) {
        long now = System.currentTimeMillis();
        long duration = dateEnd - dateBegin;

        String data = WidUtils.toJSON(dateBegin, dateEnd, type, values);

        Log.i("Wid WidDbHelper", String.format("addEntry at %s for %s s event '%s'", new Date(), duration / 1000, data));

        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues item = new ContentValues();
        item.put(EventEntry.COLUMN_NAME_DATE, now);
        item.put(EventEntry.COLUMN_NAME_DURATION, duration);
        item.put(EventEntry.COLUMN_NAME_DATA, data);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(
                EventEntry.TABLE_NAME,
                null,
                item);

    }

    public void getEntries(long dateBegin, long dateEnd, long minTime, StringBuilder json) {
        Log.i("Wid WidDbHelper", "getEntries");
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
            EventEntry.COLUMN_NAME_DURATION,
            EventEntry.COLUMN_NAME_DATA
        };

        // Define 'where' part of query.
        String selection = EventEntry.COLUMN_NAME_DATE + " > ? AND " + EventEntry.COLUMN_NAME_DATE + "<= ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(dateBegin), String.valueOf(dateEnd)};

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                EventEntry.COLUMN_NAME_DATE + " ASC";

        Cursor c = db.query(
                EventEntry.TABLE_NAME,  // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        String sep = json.length() > 0 ? "," : "";

        c.moveToFirst();
        while (!c.isAfterLast()) {
            long duration = c.getLong(0);
            String data = c.getString(1);

            if (duration >= minTime) {
                json.append(sep);
                json.append(data);
                sep = ",\n";
            }

            c.moveToNext();
        }

        c.close();
    }

    public void deleteEntries(long dateBegin, long dateEnd) {
        Log.i("Wid WidDbHelper", "deleteEntries");
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();

        // Define 'where' part of query.
        String selection = EventEntry.COLUMN_NAME_DATE + " > ? AND " + EventEntry.COLUMN_NAME_DATE + "<= ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(dateBegin), String.valueOf(dateEnd)};

        // Issue SQL statement.
        db.delete(EventEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void deleteAllEntries() {
        Log.i("Wid WidDbHelper", "deleteAllEntries");
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();

        // Issue SQL statement.
        db.delete(EventEntry.TABLE_NAME, null, null);
    }


}
