package com.codelutin.wid.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidHeartRateHelper implements SensorEventListener {

    private final static int MICROSECONDS_IN_ONE_MINUTE = 60000000;
    private final static int MIN_MICRO_BW_UPDATES = 20 * MICROSECONDS_IN_ONE_MINUTE;

    private WidDbHelper dbHelper;
    private SharedPreferences config;
    private SensorManager sm;

    private long offsetTime;

    public WidHeartRateHelper(Context context) {
        this.dbHelper = new WidDbHelper(context);
        this.config = PreferenceManager.getDefaultSharedPreferences(context);

        sm = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
    }

    public void untrackHeartRate() {
        Log.i("Wid heartrate", "untrackHeartRate");
        sm.unregisterListener(this);
    }

    public void trackHeartRate() {
        Log.i("Wid heartrate", "trackHeartRate");
        try {
            Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_HEART_RATE);
            if (sensor != null) {
                sm.registerListener(this, sensor,
                        MIN_MICRO_BW_UPDATES, MIN_MICRO_BW_UPDATES);
                Log.i("Wid heartrate", "heartrate sensor: " + sensor.getName() + "(" + sensor.getVendor() + ")");
            } else {
                Log.i("Wid heartrate", "no heartrate sensor");
            }
        } catch (Exception eee) {
            Log.e("Wid heartrate error", eee.getMessage(), eee);
        }
    }

    public void onSensorChanged(SensorEvent event) {
       Log.d("Wid heartrate", "onSensorChanged:" + event.values[0]);
       if (offsetTime == 0) {
            this.offsetTime = System.currentTimeMillis() - event.timestamp / 1000000L;
       } else {
           if (event.values[0] >= Integer.MAX_VALUE) {
               Log.i("Wid heartrate", "probably not a real value: " + event.values[0]);
           } else {
               int heartrate = (int) event.values[0];
               if (heartrate > 0) {
                   // event.timestamp is in nano convert it to millis
                   long timeInMillis = offsetTime + event.timestamp / 1000000L;
                   Log.d("Wid heartrate XXX", String.format("%s + %s / 1000000L = %s + %s = %s",
                           offsetTime, event.timestamp, offsetTime, event.timestamp / 1000000L, timeInMillis));
                   addHeartrate(heartrate, timeInMillis);
               }
           }
       }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d("Wid heartrate", sensor.getName() + " accuracy changed: " + accuracy);
    }

    private void addHeartrate(int heartrate, long now) {
        if (heartrate > 0 && heartrate < 250) {
            // save to database currentApp
            if (config.getBoolean("heartrate", true)) {
                dbHelper.addEntry(now, now, "heartrate", "pulse", String.valueOf(heartrate));
            }
        }

        Log.i("Wid Current heartrate", ""+heartrate);
    }

}
