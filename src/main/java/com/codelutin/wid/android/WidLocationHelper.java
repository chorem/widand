package com.codelutin.wid.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidLocationHelper implements LocationListener {

    // in millis
    private long MIN_TIME_BW_UPDATES = 1 * 60 * 60 * 1000; // 1h
    // in meters
    private float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1000; // 1km

    private WidDbHelper dbHelper;
    private SharedPreferences config;
    private LocationManager mLocationManager;

    private Location lastLocation;
    private long lastDate;

    public WidLocationHelper(Context context) {
        try {
            this.dbHelper = new WidDbHelper(context);
            this.config = PreferenceManager.getDefaultSharedPreferences(context);

            mLocationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        } catch (Exception eee) {
            Log.e("Wid location ERROR", eee.getMessage(), eee);
        }
    }

    public void untrackLocation() {
        try {
            Log.i("Wid location", "untrackLocation");
            mLocationManager.removeUpdates(this);
        } catch (Exception eee) {
            Log.e("Wid location ERROR", eee.getMessage(), eee);
        }
    }
    
    public void trackLocation() {
        Log.i("Wid location", "trackLocation");
        Location location = null;
        try {
            // getting GPS status
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Log.d("Wid Location", "no provider available");
            } else {
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    mLocationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER,  MIN_TIME_BW_UPDATES,  MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Wid location", "Track use Network position");
                }
                //get the location by gps
                if (isGPSEnabled && location == null) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Wid locaation", "Track use GPS position");
                }
            }

            // init with last know location
            addLocation(getPassiveLocation(), false);
        } catch (Exception eee) {
            Log.e("Wid location error", eee.getMessage(), eee);
        }
    }

    public void onLocationChanged(Location location) {
        try {
            addLocation(location, false);
        } catch (Exception eee) {
            Log.e("Wid location error", eee.getMessage(), eee);
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("Wid location onStatusChanged", String.format("provider: %s, status: %s, extras: %s", provider, status, extras));
    }

    public void onProviderEnabled(String provider) {
        Log.i("Wid location onProviderEnabled", String.format("provider: %s", provider));
        // force re-registration on network provider if become available
        untrackLocation();
        trackLocation();
    }

    public void onProviderDisabled(String provider) {
        Log.i("Wid location onProviderDisabled", String.format("provider: %s", provider));
        // force re-registration in case of current provider is not available anymore
        untrackLocation();
        trackLocation();
    }

    public void addLocation(boolean force) {
        try {
            long now = System.currentTimeMillis();
            if (force || lastDate + MIN_TIME_BW_UPDATES < now) {
                Location l = getPassiveLocation();
                if (l != null && l.getTime() + MIN_TIME_BW_UPDATES > now) {
                    // l value is computed in last year, use it
                    addLocation(l, force);
                } else if (config.getBoolean("location", true)) {
                    // try to compute new location, only if user ask for location
                    // TODO compute location 
                }
            }
        } catch(Exception eee) {
            Log.e("Wid location error", eee.getMessage(), eee);
        }
    }

    private Location getPassiveLocation() {
        Location result = mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        return result;
    }

    private void addLocation(Location location, boolean force) {
        if (force || location == null || !location.equals(lastLocation)) {
            long now = System.currentTimeMillis();
            if (lastLocation != null) {
                if (force || config.getBoolean("location", true)) {
                    dbHelper.addEntry(lastDate, now, "location",
                            "time", lastLocation.getTime(),
                            "provider", lastLocation.getProvider(),
                            "speed", lastLocation.getSpeed(),
                            "latitude", lastLocation.getLatitude(),
                            "longitude", lastLocation.getLongitude(),
                            "altitude", lastLocation.getAltitude(),
                            "accuracy", lastLocation.getAccuracy());
                }
            }
            this.lastLocation = location;
            this.lastDate = now;

            Log.i("Wid Current location", ""+location);
        }
    }
}
