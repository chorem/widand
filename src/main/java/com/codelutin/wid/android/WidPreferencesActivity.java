package com.codelutin.wid.android;


import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidPreferencesActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    static public class MyPreferenceFragment extends PreferenceFragment {

        public MyPreferenceFragment() {
        }
        
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            try {
                super.onCreate(savedInstanceState);
                addPreferencesFromResource(R.xml.preferences);

                Preference deviceNamePref = findPreference("deviceName");
                deviceNamePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Log.i("Wid preference", "deviceName: " + newValue);
                        WidAnd.DEVICE_NAME = newValue == null ? "phone" : String.valueOf(newValue);
                        return true;
                    }
                });

                Preference startStopTagPref = findPreference("startStopTag");
                startStopTagPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Log.i("Wid preference", "startStopTag: " + newValue);
                        WidAgent.widAgent.createNotification();
                        return true;
                    }
                });

                Preference locationPref = findPreference("location");
                locationPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Log.i("Wid preference", "location: " + newValue);
                        if (WidAgent.locationHelper != null) {
                            if (Boolean.TRUE.equals(newValue)) {
                                WidAgent.locationHelper.trackLocation();
                            } else {
                                WidAgent.locationHelper.untrackLocation();
                            }
                        }
                        return true;
                    }
                });

                Preference stepPref = findPreference("step");
                stepPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Log.i("Wid preference", "step: " + newValue);
                        if (WidAgent.stepHelper != null) {
                            if (Boolean.TRUE.equals(newValue)) {
                                WidAgent.stepHelper.trackStep();
                            } else {
                                WidAgent.stepHelper.untrackStep();
                            }
                        }
                        return true;
                    }
                });

                Preference heartratePref = findPreference("heartrate");
                heartratePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Log.i("Wid preference", "heartrate: " + newValue);
                        if (WidAgent.heartrateHelper != null) {
                            if (Boolean.TRUE.equals(newValue)) {
                                WidAgent.heartrateHelper.trackHeartRate();
                            } else {
                                WidAgent.heartrateHelper.untrackHeartRate();
                            }
                        }
                        return true;
                    }
                });

            } catch (Exception eee) {
                Log.e("Wid preference ERROR", eee.getMessage(), eee);
            }
        }
    }

}