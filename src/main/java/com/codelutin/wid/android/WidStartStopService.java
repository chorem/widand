package com.codelutin.wid.android;


import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidStartStopService extends IntentService {

    private WidDbHelper dbHelper;
    private SharedPreferences config;

    public WidStartStopService() {
        super("WidStartStopService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.dbHelper = new WidDbHelper(this);
        config = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("Wid WidStartStopService", "created");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String action = intent.getStringExtra("action");
            String tag = intent.getStringExtra("tag");
            if (action != null && tag != null) {
                long startDate = config.getLong(tag, 0);
                long date = System.currentTimeMillis();
                dbHelper.addEntry(startDate > 0 ? startDate : date, date, "mark",
                        "tags", ("\""+ TextUtils.join("\" \"", tag.split(" ")) + "\"").split(" "),
                        "action", action);

                if ("stop".equals(action)) {
                    date = 0;
                }
                SharedPreferences.Editor editor = config.edit();
                editor.putLong(tag, date);
                editor.commit();

                Log.d("Wid WidStartStopService", "tag added:" + tag + ":" + action + ":" + date + ":" + config.getLong(tag, -1));
                WidAgent.widAgent.createNotification();
            }
        } catch (Exception eee) {
            Log.e("Wid autosync ERROR", eee.getMessage(), eee);
        }
    }


}
