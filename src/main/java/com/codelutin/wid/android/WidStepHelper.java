package com.codelutin.wid.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidStepHelper implements SensorEventListener {

    private final static int MICROSECONDS_IN_ONE_MINUTE = 60000000;
    private final static int MIN_MICRO_BW_UPDATES = 5 * MICROSECONDS_IN_ONE_MINUTE;

    private WidDbHelper dbHelper;
    private SharedPreferences config;
    private SensorManager sm;

    private long offsetTime;
    private long lastDate;
    private int lastStep;

    public WidStepHelper(Context context) {
        this.dbHelper = new WidDbHelper(context);
        this.config = PreferenceManager.getDefaultSharedPreferences(context);

        sm = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
    }

    public void untrackStep() {
        Log.i("Wid step", "untrackStep");
        sm.unregisterListener(this);
    }

    public void trackStep() {
        Log.i("Wid step", "trackStep");
        try {
            Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            if (sensor != null) {
                sm.registerListener(this, sensor,
                        MIN_MICRO_BW_UPDATES, MIN_MICRO_BW_UPDATES);
                Log.i("Wid step", "Step sensor: " + sensor.getName() + "(" + sensor.getVendor() + ")");
            } else {
                Log.i("Wid step", "no step sensor");
            }
        } catch (Exception eee) {
            Log.e("Wid step error", eee.getMessage(), eee);
        }
    }

    public void onSensorChanged(SensorEvent event) {
       Log.d("Wid step", "onSensorChanged:" + event.values[0]);
       if (offsetTime == 0) {
            this.offsetTime = System.currentTimeMillis() - event.timestamp / 1000000L;
       } else {
           if (event.values[0] >= Integer.MAX_VALUE) {
               Log.i("Wid step", "probably not a real value: " + event.values[0]);
           } else {
               int steps = (int) event.values[0];
               if (steps > 0) {
                   // event.timestamp is in nano convert it to millis
                   long timeInMillis = offsetTime + event.timestamp / 1000000L;
                   Log.d("Wid step XXX", String.format("%s + %s / 1000000L = %s + %s = %s",
                           offsetTime, event.timestamp, offsetTime, event.timestamp / 1000000L, timeInMillis));
                   addStep(steps, timeInMillis);
               }
           }
       }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d("Wid step", sensor.getName() + " accuracy changed: " + accuracy);
    }

    private void addStep(int allStep, long now) {
        int step = allStep - lastStep;
        if (lastDate == 0) {
            this.lastDate = now;
            this.lastStep = allStep;
        } else if (step > 0 && now - lastDate >= MIN_MICRO_BW_UPDATES / 1000) {
            // save to database currentApp
            if (config.getBoolean("step", true)) {
                dbHelper.addEntry(
                        lastDate, now, "step",
                        "number", step);
            }
            this.lastDate = now;
            this.lastStep = allStep;
        }

        Log.i("Wid Current step", ""+step + String.format(" %s - %s (%s) >= %s", now, lastDate, now - lastDate, MIN_MICRO_BW_UPDATES / 1000));
    }

}
