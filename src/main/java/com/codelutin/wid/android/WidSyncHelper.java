package com.codelutin.wid.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidSyncHelper {

    private WidDbHelper dbHelper;
    private WidCallLogHelper callHelper;
    private WidCalendarHelper calendarHelper;
    private ConnectivityManager connMgr;
    private WidLocationHelper locationHelper;

    private SharedPreferences config;
    
    /**
     *      Context context = getBaseContext();
     *      ConnectivityManager connMgr =
     *                  (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
     *
     * @param context
     */
    public WidSyncHelper(Context context) {
        this.connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.callHelper = new WidCallLogHelper(context);
        this.dbHelper = new WidDbHelper(context);
        this.calendarHelper = new WidCalendarHelper(context);
        this.locationHelper = new WidLocationHelper(context);

        config = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void getJson(StringBuilder json) {
        long syncDate = config.getLong("syncDate", 0);
        long now = System.currentTimeMillis();

        getJson(syncDate, now, json);
    }

    public void getJson(long begin, long end, StringBuilder json) {
        long minTime = Long.parseLong(config.getString("minTime", "0")) * 60 * 1000;

        // add location if needed
        locationHelper.addLocation(false);

        dbHelper.getEntries(begin, end, minTime, json);
        if (config.getBoolean("call", true)) {
            callHelper.getCallDetails(begin, end, json);
        }
        if (config.getBoolean("calendar", true)) {
            calendarHelper.getCalendarEvents(begin, end, json);
        }

        // must done after list generation, because helper use json length to
        // know if there is already item and add ',' before the first item
        json.insert(0, "[");
        json.append("]");
    }

    /**
     *
     * @param force
     */
    public String sync(boolean force) {
        String result = "ok";
        try {
            Log.i("Wid sync", ""+force);

            String syncType = config.getString("syncType","all");
            long syncDate = config.getLong("syncDate", 0);
            long now = System.currentTimeMillis();

            boolean mustSync = false;
            if (force || !"none".equals(syncType)) {
                NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
                if (activeInfo != null && activeInfo.isConnected()) {
                    mustSync = force || "all".equals(syncType)
                            || activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
                }
            }

            if (mustSync) {
                String widurl = config.getString("url", "NA");
                
                StringBuilder json = new StringBuilder();
                getJson(syncDate, now, json);

                // TODO send all available data
                Log.i("Wid Sync url", widurl);
                Log.i("Wid Sync data", json.toString());

                URL url = new URL(widurl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    byte[] bytes = json.toString().getBytes("UTF-8");
//                    urlConnection.setFixedLengthStreamingMode(bytes.length);
                    OutputStream os = urlConnection.getOutputStream();
                    os.write(bytes);
                    os.flush();
                    
                    int code = urlConnection.getResponseCode();
                    if (code >= 200 && code <= 300) {
                        SharedPreferences.Editor editor = config.edit();
                        editor.putLong("syncDate", now);
                        editor.commit();

                        dbHelper.deleteEntries(syncDate, now);
                    } else {
                        result = "sync error: " + code;
                    }
                } finally {
                    urlConnection.disconnect();
                }
            }
        } catch (Exception eee) {
            Log.e("Wid sync error", eee.getMessage(), eee);
            StringWriter sw = new StringWriter();
            eee.printStackTrace(new PrintWriter(sw, true));
            result = sw.toString();
        }
        return result;
    }

}
