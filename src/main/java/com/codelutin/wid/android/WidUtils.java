package com.codelutin.wid.android;

import android.text.TextUtils;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WidUtils {

    public static String escapeJSON(Object s) {
        if (s == null) {
            s = "";
        }
        return s.toString().replaceAll("\n", "|").replaceAll("\"", "\\\"");
    }

    public static String toJSON(long beginDateTime, long endDateTime, String type, Object...data) {

        String result = String.format("{"
                + "\"beginDateTime\": %s, "
                + "\"endDateTime\": %s, "
                + "\"type\": \"%s\", "
                + "\"source\": \"%s\","
                + "\"data\": {"
                +     "\"device\": \"%s\"",
                beginDateTime, endDateTime, type, WidAnd.APP_NAME, WidAnd.DEVICE_NAME);

        for (int i = 0, maxi = data.length; i < maxi;) {
            Object key = data[i++];
            Object val = data[i++];
            if (val instanceof Number) {
                result += String.format(", \"%s\": %s", key, val);
            } else if (val != null && val.getClass().isArray()) {
                result += String.format(", \"%s\": %s", key, "[" + escapeJSON(TextUtils.join(", ", (Object[])val)) + "]");
            } else {
                result += String.format(", \"%s\": \"%s\"", key, escapeJSON(val));
            }
        }

        result += "}}";

        return result;
    }
}
